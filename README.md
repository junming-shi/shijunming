# 训练书法字体图像获取

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明

####  功能
图像预处理：
将输入图像转换为灰度图像
使用Otsu阈值法进行二值化处理
应用形态学操作（腐蚀、膨胀、闭运算）以增强字符轮廓
边缘检测和轮廓提取：
使用高斯模糊和Canny边缘检测算法
查找图像中的轮廓
汉字分割：
根据轮廓周长筛选有效的汉字区域
为每个检测到的汉字绘制边界框
将每个分割出的汉字保存为单独的图像文件
可视化处理过程：
使用Matplotlib创建多个子图，展示处理的各个步骤

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
本项目依赖以下库：
OpenCV==4.9.0.80
numpy==1.24.3
matplotlib==3.7.2
请确保你安装了所有正确版本的依赖库
个人信息
姓名：石峻名
学号: 202252320409
年级: 2022
专业: 智能科学与技术
班级: 4班`

这里输入代码`
import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
plt.rcParams['font.sans-serif'] = ['SimHei'] 
plt.rcParams['axes.unicode_minus'] = False
img = cv2.imread('hanzi1.jpg', 0)
img1=cv2.imread('hanzi1.jpg')
thresh_hold, new_img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
thresh_inv = cv2.bitwise_not(new_img)
kernel = np.ones((3, 3), np.uint8)
eroded_img = cv2.erode(thresh_inv, kernel,iterations=4)
kernel1 = np.ones((15,15), np.uint8)
dilated_img = cv2.dilate(eroded_img, kernel1,iterations=3)
kernel2 = np.ones((75, 75), np.uint8)
closed_img = cv2.morphologyEx(dilated_img , cv2.MORPH_CLOSE, kernel2)
lower = 100
upper = 300
img_blur = cv2.GaussianBlur(closed_img,(5,5), 0) #高斯滤波
edges_with_blur = cv2.Canny(img_blur, lower, upper)
contours, hierarchy = cv2.findContours(edges_with_blur, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# 遍历轮廓列表
for index, c in enumerate(contours):
    perimeter = cv2.arcLength(c, True)
    x, y, w, h = cv2.boundingRect(c)
    # 根据周长阈值筛选并绘制边界框
    if perimeter > 100:
        char_img = img[y:y + h, x:x + w]  # 提取汉字图像
        char_filename = os.path.join('chars', f'char_{index + 1}.jpg')  # 生成文件名
        cv2.imwrite(char_filename, char_img)  # 保存汉字图像
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
fg, ax = plt.subplots(1, 7, figsize=(15, 5))
ax[0].imshow(img1, cmap='gray')  
ax[0].set_title("原始图像(灰度图)")  
ax[1].imshow(thresh_inv, cmap='gray')  
ax[1].set_title("二值图像")
ax[2].imshow(eroded_img, cmap='gray') 
ax[2].set_title("腐蚀后的图像")
ax[3].imshow(dilated_img,cmap='gray')
ax[3].set_title('膨胀后的图像')
ax[4].imshow(closed_img,cmap='gray')
ax[4].set_title('闭运算后的图像')
ax[5].imshow(edges_with_blur,cmap='gray')
ax[5].set_title('高斯模糊后的图像')
ax[6].imshow(img,cmap='gray')
ax[6].set_title('识别后的图像')
# 自动调整子图参数，使子图填充整个图像区域
plt.tight_layout()
# 显示图像
plt.show()